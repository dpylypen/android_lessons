package com.dimas.p0111resvalues;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvTop;
    TextView tvBottom;
    Button btnTop;
    Button btnBottom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LinearLayout llBottom = (LinearLayout) findViewById(R.id.llBottom);
        tvBottom = (TextView) findViewById(R.id.tvBottom);
        btnBottom = (Button) findViewById(R.id.btnBottom);
        tvTop = (TextView) findViewById(R.id.tvTop);
        btnTop = (Button) findViewById((R.id.btnTop));

        llBottom.setBackgroundColor(R.color.llBottomColor);
        tvBottom.setText(R.string.tvBottomText);
        btnBottom.setText(R.string.btnBottomText);

        btnTop.setOnClickListener(this);
        btnBottom.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnTop:
                tvBottom.setText("Нажата кнопка TOP");
                tvTop.setText("Нажата кнопка TOP");
                break;
            case R.id.btnBottom:
                tvBottom.setText("Нажата кнопка BOTTOM");
                tvTop.setText("Нажата кнопка BOTTOM");
                break;
        }
    }
}
