package com.dimas.p0152contextmenuhw;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    TextView tvColor;
    // MenuItem r;
    // MenuItem g;
    // MenuItem b;
    //  ContextMenu mymenu = (ContextMenu) R.menu.mymenu;
    // Button r;
    // Button g;
    // Button b;
   // final char MENU_COLOR_RED = 'r';
  //  final char MENU_COLOR_GREEN = 'g';
  //  final char MENU_COLOR_BLUE = 'b';

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvColor = (TextView) findViewById(R.id.tvColor);
        // r = (Button) findViewById(R.id.r);
        // g = (Button) findViewById(R.id.g);
        // b = (Button) findViewById(R.id.b);

        tvColor.setOnCreateContextMenuListener(oCCML);

        //registerForContextMenu(tvColor);


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    View.OnCreateContextMenuListener oCCML = new View.OnCreateContextMenuListener() {
        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            getMenuInflater().inflate(R.menu.mymenu, contextMenu);
        }
    };

   /* @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.mymenu, menu);
    }
    */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            // пункты меню для tvColor
            case R.id.r:
                tvColor.setTextColor(Color.RED);
                tvColor.setText("Text color = red");
                break;
            case R.id.g:
                tvColor.setTextColor(Color.GREEN);
                tvColor.setText("Text color = green");
                break;
            case R.id.b:
                tvColor.setTextColor(Color.BLUE);
                tvColor.setText("Text color = blue");
                break;
        }
        return super.onContextItemSelected(item);
    }
}








